﻿using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using NetworkChat.Net;
using NetworkChat.Net.Message;
using NetworkChat.Net.Profile;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using MessageBox = System.Windows.Forms.MessageBox;

namespace NetworkChat {
    /// <summary>
    ///     Interaction logic for loginWIndow.xaml
    /// </summary>
    public partial class LoginWindow : Window {
        public LoginWindow (){
            InitializeComponent ();
        }

        private void btnLogin_Click ( object sender, RoutedEventArgs e ){
            if (TbxAccount.Text.Length < 3){
                MessageBox.Show( Properties.Resources.Account_name_is_too_short, Properties.Resources.Internet_Real_Chat, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (TbxPassword.Password.Length < 3){
                MessageBox.Show( Properties.Resources.Password_is_too_short, Properties.Resources.Internet_Real_Chat, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var queryClient = new QueryClient ();
            Account.Name = TbxAccount.Text;
            Account.Password = TbxPassword.Password;

            ClientMessage clientMessage = queryClient.SendQuery ( new BitBuffer ( Tokens.Login, true ) );

            if (!queryClient.IsConnected){
                MessageBox.Show( Properties.Resources.Connection_to_server_failed, Properties.Resources.Internet_Real_Chat, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (clientMessage["token"] != "7"){
                return;
            }

            if (clientMessage["login_status"] == "True"){
                Hide ();
                Account.AccessLevel = clientMessage["access_level"];
                var window = new RoomWindow{Owner = this};
                window.Show ();
            }
            else{
                MessageBox.Show( Properties.Resources.Account_Name_Account_Password_is_wrong, Properties.Resources.Internet_Real_Chat, MessageBoxButtons.OK,
                                  MessageBoxIcon.Error );
            }
        }

        private void tbxAccount_GotFocus ( object sender, RoutedEventArgs e ){
            if (TbxAccount.Text == (string) TbxAccount.Tag){
                TbxAccount.Foreground = Brushes.Black;
                TbxAccount.Clear ();
            }
        }

        private void tbxPassword_GotFocus ( object sender, RoutedEventArgs e ){
            if (TbxPassword.Password == (string) TbxPassword.Tag){
                TbxPassword.Clear ();
            }
        }

        private void tbxPassword_LostFocus ( object sender, RoutedEventArgs e ){
            if (TbxPassword.Password.Length == 0){
                TbxPassword.Password = (string) TbxPassword.Tag;
            }
        }

        private void tbxAccount_LostFocus ( object sender, RoutedEventArgs e ){
            if (TbxAccount.Text.Length == 0){
                TbxAccount.Text = (string) TbxAccount.Tag;
                TbxAccount.Foreground = Brushes.LightGray;
            }
        }

        private void btnRegister_Click ( object sender, RoutedEventArgs e ){
            var window = new RegistrationWindow{Owner = this};
            window.ShowDialog ();
        }

        private void tbxPassword_PreviewKeyDown ( object sender, KeyEventArgs e ){
            if (e.Key == Key.Enter){
                btnLogin_Click ( null, null );
            }
        }

        private void tbxAccount_PreviewKeyDown ( object sender, KeyEventArgs e ){
            if (e.Key == Key.Enter){
                btnLogin_Click ( null, null );
            }
        }

        public void SetProperties ( string name, string password ){}
    }
}