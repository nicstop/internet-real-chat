﻿using System.Windows;
using System.Windows.Forms;
using NetworkChat.Net;
using NetworkChat.Net.Message;
using MessageBox = System.Windows.Forms.MessageBox;

namespace NetworkChat {
    /// <summary>
    ///     Interaction logic for passwordWindow.xaml
    /// </summary>
    public partial class PasswordWindow {
        private readonly QueryClient _queryClient;
        private readonly string _room;

        public PasswordWindow (){
            InitializeComponent ();
        }

        public PasswordWindow ( QueryClient queryClient, string room ){
            InitializeComponent ();

            _queryClient = queryClient;
            _room = room;
        }


        private void btnCheck_Click ( object sender, RoutedEventArgs e ){
            if (passwordBox1.Password == ""){
                return;
            }

            var message =
                _queryClient.SendQuery ( new BitBuffer ( Tokens.JoinRoom, true, "room_name=" + _room,
                                                         "room_password=" + passwordBox1.Password ) );
            ProcessData ( message );
        }

        private void ProcessData ( ClientMessage clientMessage ){
            var token = Token.Parse ( clientMessage["token"] == "" ? "" : clientMessage["token"] );

            if (token != Tokens.JoinRoom){
                return;
            }

            switch (clientMessage["join_status"]){
                case "room_not_found":
                    MessageBox.Show( Properties.Resources.Room_was_not_found, Properties.Resources.Internet_Real_Chat, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case "require_password":
                    passwordBox1.Clear ();
                    MessageBox.Show( Properties.Resources.Wrong_password, Properties.Resources.Internet_Real_Chat, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case "room_full":
                    MessageBox.Show( Properties.Resources.Room_is_full_, Properties.Resources.Internet_Real_Chat, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case "enter_room":
                    new ChatWindow ( new Connection ( _queryClient.Handler ), _room ).Show ();
                    Close ();
                    break;
            }
        }

        private void Window_Loaded ( object sender, RoutedEventArgs e ){
            Title += _room;
        }
    }
}