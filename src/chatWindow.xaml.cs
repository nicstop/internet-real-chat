﻿using System;
using System.Collections;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using NetworkChat.Net;
using NetworkChat.Net.Message;
using NetworkChat.Net.Profile;
using NetworkChat.Properties;
using UIElements;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using MessageBox = System.Windows.Forms.MessageBox;

namespace NetworkChat {

    /// <summary>
    ///     Interaction logic for chatWindow.xaml
    /// </summary>
    public partial class ChatWindow : Window {

        private readonly string _room;
        private string _accessLevel;

        public ChatWindow(){
            InitializeComponent();
        }

        public ChatWindow( Connection connection, string room ){
            InitializeComponent();
            RoomIrc = new RoomIrc( connection, Account.Name );
            _room = room;
        }

        private RoomIrc RoomIrc { get; set; }

        private void btnSend_Click( object sender, RoutedEventArgs e ){
            Send( TbInput.Text );
        }

        public void Send( string source ){
            if (string.IsNullOrEmpty( source )){
                return;
            }

            RoomIrc.Send( new BitBuffer( Tokens.SendRoomClients,
                    "data=" +
                            source.Replace( "=", "equals_smbl" )
                                    .Replace( "&", "and_smbl" )
                                    .Replace( "<EOF>", "eof_smbl" ),
                    "color=" + Settings.Default.MessageColor ) );
            TbInput.Clear();
        }

        private void Window_Loaded( object sender, RoutedEventArgs e ){
            RoomIrc.RecivedData += UpdateOutput;
            RoomIrc.Send( new BitBuffer( Tokens.ClientList ) );
            RoomIrc.Send( new BitBuffer( Tokens.WhoIam ) );
            RoomIrc.Send( new BitBuffer( Tokens.NoticeConnection ) );
            RoomIrc.Send( new BitBuffer( Tokens.AddClient ) );
            Title = string.Format( "You are in room \"{0}\"", _room );
        }

        private void UpdateOutput( object sender, ConnectionDataEventArgs e ){
            var queue = (Queue) sender;

            while (queue.Count != 0){
                ClientMessage clientMessage = new ClientMessage().Parse( (string) queue.Dequeue() );

                try{
                    switch (Token.Parse( clientMessage["token"] == "" ? "0" : clientMessage["token"] )){
                        case Tokens.SendRoomClients:
                            RtbOutput.Dispatcher.Invoke(
                                                        new Callback(
                                                                s =>
                                                                        RTBNavigationService.OnContentChanged(
                                                                                                              RtbOutput,
                                                                                new DependencyPropertyChangedEventArgs(
                                                                                        RTBNavigationService
                                                                                                .ContentProperty,
                                                                                        null, s ) ) ),
                                    clientMessage["color"] + "|" + clientMessage["data"] );
                            break;
                        case Tokens.AddClient:
                            InRoomList.Dispatcher.Invoke(
                                                         new Callback( s => InRoomList.Items.Add( new{
                                                                 in_room = s
                                                         } ) ),
                                    new object[]{
                                            clientMessage["data"]
                                    } );
                            break;
                        case Tokens.RemoveClient:
                            InRoomList.Dispatcher.Invoke(
                                                         new Callback( s => InRoomList.Items.Remove( new{
                                                                 in_room = s
                                                         } ) ),
                                    new object[]{
                                            clientMessage["data"]
                                    } );
                            break;
                        case Tokens.ClientList:
                            InRoomList.Dispatcher.Invoke(
                                                         new Callback( s => s.Split( new[]{
                                                                 ","
                                                         },
                                                                 StringSplitOptions.RemoveEmptyEntries )
                                                                 .ToList()
                                                                 .ForEach( item => InRoomList.Items.Add( new{
                                                                         in_room = item
                                                                 } ) ) ),
                                    new object[]{
                                            clientMessage["data"].Replace( "comma_smbl", "," )
                                    } );
                            break;
                        case Tokens.WhoIam:
                            _accessLevel = clientMessage["data"];
                            if (_accessLevel != "0"){
                                RoomOptionsMenu.Dispatcher.Invoke(
                                                                  new Callback(
                                                                          ( s ) =>
                                                                                  RoomOptionsMenu.Visibility =
                                                                                          Visibility.Visible ),
                                        _accessLevel );
                            }
                            break;
                        case Tokens.AccountOwnerEntered:
                            MessageBox.Show(
                                            Properties.Resources.Account_Owner_Entered,
                                    Properties.Resources.Internet_Real_Chat, MessageBoxButtons.OK, MessageBoxIcon.Error );
                            Dispatcher.Invoke( new Callback( s => Close() ), clientMessage["data"] );
                            break;
                        case Tokens.Banned:
                            MessageBox.Show( Properties.Resources.You_are_banned_out_of_this_room,
                                    Properties.Resources.Internet_Real_Chat, MessageBoxButtons.OK,
                                    MessageBoxIcon.Error );
                            Dispatcher.Invoke( new Callback( s => Close() ), clientMessage["data"] );
                            break;
                        case Tokens.Kicked:
                            MessageBox.Show( Properties.Resources.You_were_kicked_out_of_this_room );
                            Dispatcher.Invoke( new Callback( s => Close() ), clientMessage["data"] );
                            break;
                    }
                }
                catch (InvalidOperationException){}
            }
        }

        private void Window_Unloaded( object sender, RoutedEventArgs e ){
            RoomIrc.Send( new BitBuffer( Tokens.CloseMe ) );
            RoomIrc.Disconnect();
        }

        private void TbInput_PreviewKeyDown( object sender, KeyEventArgs e ){
            if (e.Key != Key.Enter){
                return;
            }
            e.Handled = true;
            Send( TbInput.Text );
        }

        private void MenuItem_Click( object sender, RoutedEventArgs e ){
            var banW = new BanWindow( RoomIrc, null ){
                    Owner = this
            };
            banW.ShowDialog();
        }

        private void MenuItem_Click_1( object sender, RoutedEventArgs e ){
            var cpWindow = new ColorPickerWindow{
                    Owner = this
            };
            cpWindow.ShowDialog();
        }

        private delegate void Callback( string data );

        private void MenuItem_OnClick( object sender, RoutedEventArgs e ){
            new KickWindow( RoomIrc ){
                    Owner = this
            }.ShowDialog();
        }


        private void UnbanMenuItem_Click( object sender, RoutedEventArgs e ){
            new UnbanWindow( RoomIrc ){
                    Owner = this
            }.ShowDialog();
        }
    }

}