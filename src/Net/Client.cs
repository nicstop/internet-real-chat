﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using NetworkChat.Common.Extensions;
using NetworkChat.Net.Message;

namespace NetworkChat.Net {
    public class Client : IDisposable {
        public delegate void ConnectionDataEventHandler ( object sender, ConnectionDataEventArgs e );

        public delegate void ConnectionEventHandler ( object sender, ConnectionEventArgs e );

        private static readonly byte[] _endOfData = Encoding.UTF8.GetBytes ( "<EOF>" );

        private bool _disposed;

        public Client ( Connection connection ){
            Connection = connection;
            Connection.BeginReceive ( RecvieCallback, null );
        }

        public Connection Connection { get; private set; }

        public void Dispose (){
            Dispose ( true );
        }

        protected event ConnectionDataEventHandler DataRecived;
        protected event ConnectionEventHandler OnDisconnect;

        private void RecvieCallback ( IAsyncResult result ){
            if (_disposed || Connection == null){
                return;
            }

            try{
                int byteRecv = Connection.EndReceive ( result );

                if (byteRecv > 0){
                    byte[] RecvBuffer = Connection.RecvBuffer.Enumerate ( 0, byteRecv ).ToArray ();

                    if (!RecvBuffer.Contains ( _endOfData, 0 )){
                        RecvBuffer = RecvBuffer.Append ( Connection.ReceiveBigData ( _endOfData ) );
                    }

                    OnRecive ( RecvBuffer, new ConnectionDataEventArgs ( Connection, RecvBuffer ) );
                    Connection.BeginReceive ( RecvieCallback, null );
                }
                else{
                    OnClientDisconnect ( null, new ConnectionEventArgs ( Connection ) ); // Connection was lost.
                }
            }
            catch (NullReferenceException){
                OnClientDisconnect ( null, new ConnectionEventArgs ( Connection ) );
            }
            catch (ObjectDisposedException){}
            catch (SocketException){
                Debug.WriteLine ( "Closing connection." );
                Dispose ();
            }
        }

        /// <summary>
        ///     Send BitBuffer to remote endpoint.
        /// </summary>
        public virtual void Send ( BitBuffer bitBuffer ){
            if (_disposed || Connection == null){
                return;
            }

            try{
                Connection.SendBigData ( bitBuffer );
            }
            catch (SocketException){
                OnClientDisconnect ( null, new ConnectionEventArgs ( Connection ) );
            }
            catch (NullReferenceException){
                OnClientDisconnect ( null, new ConnectionEventArgs ( Connection ) );
            }
        }

        public virtual void Disconnect (){
            OnClientDisconnect ( null, new ConnectionEventArgs ( Connection ) );
        }

        protected virtual void Dispose ( bool disposing ){
            if (_disposed){
                return;
            }

            if (disposing){
                Connection.Disconnect ();
            }

            // Dispose of unmanaged resources here.
            _disposed = true;
        }

        #region events

        private void OnRecive ( object sender, ConnectionDataEventArgs e ){
            if (DataRecived != null){
                DataRecived ( sender, e );
            }
        }

        private void OnClientDisconnect ( object sender, ConnectionEventArgs e ){
            if (OnDisconnect != null){
                OnDisconnect ( sender, e );
            }
        }

        #endregion
    }
}