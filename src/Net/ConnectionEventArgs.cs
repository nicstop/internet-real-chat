﻿using System;

namespace NetworkChat.Net {
    public class ConnectionEventArgs : EventArgs {
        public ConnectionEventArgs ( IConnection connection ){
            if (connection == null){
                throw new ArgumentNullException ( "connection" );
            }
            Connection = connection;
        }

        public IConnection Connection { get; private set; }

        public override string ToString (){
            return Connection.RemoteEndPoint != null
                       ? Connection.RemoteEndPoint.ToString ()
                       : "Not Connected";
        }
    }
}