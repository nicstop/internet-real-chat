﻿using System;
using System.Collections;
using System.Linq;
using System.Text;

namespace NetworkChat.Net {
    public class RoomIrc : Client {
        public RoomIrc ( Connection connection, string accountName ) : base ( connection ){
            if (connection == null){
                throw new ArgumentNullException ( "connection" );
            }
            if (accountName == null){
                throw new ArgumentNullException ( "accountName" );
            }

            Name = accountName;

            DataRecived += RoomClient_DataRecived; // Subscribe on event.
        }

        public string Name { get; private set; }
        public event ConnectionDataEventHandler RecivedData;

        private void RoomClient_DataRecived ( object sender, ConnectionDataEventArgs e ){
            Queue queue = CreateQueue ( e.Data.ToArray () );
            OnRecive ( queue, e );
        }

        private Queue CreateQueue ( byte[] array ){
            var queue = new Queue ();

            Encoding.UTF8.GetString ( array )
                    .Split ( new[]{"<EOF>"}, StringSplitOptions.RemoveEmptyEntries )
                    .ToList ()
                    .ForEach ( item => queue.Enqueue ( item ) );

            return queue;
        }

        #region events

        private void OnRecive ( object sender, ConnectionDataEventArgs e ){
            if (RecivedData != null){
                RecivedData ( sender, e );
            }
        }

        #endregion
    }
}