﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using NetworkChat.Common.Extensions;
using NetworkChat.Net.Message;

namespace NetworkChat.Net {
    /// <summary>
    ///     TCP Connection class.
    /// </summary>
    public class Connection : IConnection {
        /// <summary>
        ///     Default buffer size.
        /// </summary>
        public static readonly int BufferSize = 1024; // 1 KB       

        /// <summary>
        ///     The recieve buffer.
        /// </summary>
        private readonly byte[] _recvBuffer = new byte[BufferSize];

        public Connection ( Socket socket ){
            if (socket == null){
                throw new ArgumentNullException ( "socket" );
            }

            Socket = socket;
        }

        #region socket stuff

        /// <summary>
        ///     Returns true if there exists an active connection.
        /// </summary>
        public bool IsConnected{
            get { return (Socket == null) ? false : Socket.Connected; }
        }

        /// <summary>
        ///     Returns remote endpoint.
        /// </summary>
        public IPEndPoint RemoteEndPoint{
            get { return (Socket == null) ? null : Socket.RemoteEndPoint as IPEndPoint; }
        }

        /// <summary>
        ///     Returns local endpoint.
        /// </summary>
        public IPEndPoint LocalEndPoint{
            get { return Socket.LocalEndPoint as IPEndPoint; }
        }

        /// <summary>
        ///     Returns the recieve-buffer.
        /// </summary>
        public byte[] RecvBuffer{
            get { return _recvBuffer; }
        }

        // Read bytes from the Socket into the buffer in a non-blocking call.
        // This allows us to read no more than the specified count number of bytes.\
        // Note that this method should only be called prior to encryption!
        public int Receive ( int start, int count ){
            return Socket.Receive ( _recvBuffer, start, count, SocketFlags.None );
        }

        public int _Send ( BitBuffer bitBuffer ){
            return Socket.Send ( bitBuffer.Data, 0, bitBuffer.Data.Length, SocketFlags.None );
        }

        // Wrapper for the Send method that will send the data either to the
        // Socket (unecnrypted) or to the TLSStream (encrypted).
        public int _Send ( byte[] buffer, int start, int count, SocketFlags flags ){
            return Socket.Send ( buffer, start, count, flags );
        }

        /// <summary>
        ///     Sends byte buffer to remote endpoint.
        /// </summary>
        /// <param name="buffer">Byte buffer to send.</param>
        /// <returns>Returns count of sent bytes.</returns>
        public int Send ( byte[] buffer ){
            if (buffer == null){
                throw new ArgumentNullException ( "buffer" );
            }
            return Send ( buffer, 0, buffer.Length, SocketFlags.None );
        }

        /// <summary>
        ///     Sends byte buffer to remote endpoint.
        /// </summary>
        /// <param name="buffer">Byte buffer to send.</param>
        /// <param name="flags">Sockets flags to use.</param>
        /// <returns>Returns count of sent bytes.</returns>
        public int Send ( byte[] buffer, SocketFlags flags ){
            if (buffer == null){
                throw new ArgumentNullException ( "buffer" );
            }
            return Send ( buffer, 0, buffer.Length, flags );
        }

        /// <summary>
        ///     Sends byte buffer to remote endpoint.
        /// </summary>
        /// <param name="buffer">Byte buffer to send.</param>
        /// <param name="start">Start index to read from buffer.</param>
        /// <param name="count">Count of bytes to send.</param>
        /// <returns>Returns count of sent bytes.</returns>
        public int Send ( byte[] buffer, int start, int count ){
            if (buffer == null){
                throw new ArgumentNullException ( "buffer" );
            }
            return Send ( buffer, start, count, SocketFlags.None );
        }

        /// <summary>
        ///     Sends byte buffer to remote endpoint.
        /// </summary>
        /// <param name="buffer">Byte buffer to send.</param>
        /// <param name="start">Start index to read from buffer.</param>
        /// <param name="count">Count of bytes to send.</param>
        /// <param name="flags">Sockets flags to use.</param>
        /// <returns>Returns count of sent bytes.</returns>
        public int Send ( byte[] buffer, int start, int count, SocketFlags flags ){
            if (buffer == null){
                throw new ArgumentNullException ( "buffer" );
            }

            return _Send ( buffer, start, count, flags );
        }

        /// <summary>
        ///     Sends an enumarable byte buffer to remote endpoint.
        /// </summary>
        /// <param name="data">Enumrable byte buffer to send.</param>
        /// <returns>Returns count of sent bytes.</returns>
        public int Send ( IEnumerable<byte> data ){
            if (data == null){
                throw new ArgumentNullException ( "data" );
            }
            return Send ( data, SocketFlags.None );
        }

        /// <summary>
        ///     Sends an enumarable byte buffer to remote endpoint.
        /// </summary>
        /// <param name="data">Enumrable byte buffer to send.</param>
        /// <param name="flags">Sockets flags to use.</param>
        /// <returns>Returns count of sent bytes.</returns>
        public int Send ( IEnumerable<byte> data, SocketFlags flags ){
            if (data == null){
                throw new ArgumentNullException ( "data" );
            }

            return _Send ( data.ToArray (), 0, data.Count (), flags );
        }

        /// <summary>
        ///     Kills the connection to remote endpoint.
        /// </summary>
        public void Disconnect (){
            if (Socket != null){
                try{
                    Socket.Shutdown ( SocketShutdown.Both );
                    Socket.Close ();
                }
                catch (Exception){
                    // Ignore any exceptions that might occur during attempt to close the Socket.
                }
                finally{
                    Socket = null;
                }
            }
        }

        /// <summary>
        ///     Begins recieving data async.
        /// </summary>
        /// <param name="callback">Callback function be called when recv() is complete.</param>
        /// <param name="state">State manager object.</param>
        /// <returns>
        ///     Returns <see cref="IAsyncResult" />
        /// </returns>
        public IAsyncResult BeginReceive ( AsyncCallback callback, object state ){
            return Socket.BeginReceive ( _recvBuffer, 0, BufferSize, SocketFlags.None, callback, state );
        }

        public int EndReceive ( IAsyncResult result ){
            return Socket.EndReceive ( result );
        }

        public void SendBigData ( BitBuffer bitBuffer ){
            if (bitBuffer.Data.Length > BufferSize) // Check size of bytes.
            {
                bitBuffer.Data.MakeBlocks ( BufferSize )
                         .ToList ()
                         .ForEach ( block => _Send ( block, 0, block.Length, SocketFlags.None ) );
                    // Spliting on blocks and sending to client.
            }
            else{
                _Send ( bitBuffer.Data, 0, bitBuffer.Data.Length, SocketFlags.None );
            }
        }

        public byte[] ReceiveBigData ( byte[] endOfData ){
            int byteRecv = Receive ( 0, BufferSize );

            if (byteRecv > 0){
                byte[] outData = RecvBuffer.Enumerate ( 0, byteRecv ).ToArray ();
                int startPattern = 0;

                while (!outData.Contains ( endOfData, startPattern )) // If not all bytes came.
                {
                    byteRecv = Receive ( 0, RecvBuffer.Length ); // Receiving bytes.
                    outData = outData.Append ( RecvBuffer.Enumerate ( 0, byteRecv ).ToArray () );
                    startPattern = outData.Length - byteRecv; // Speedup process.
                }

                return outData;
            }

            return null;
        }

        public int Send ( BitBuffer bitBuffer ){
            if (bitBuffer == null){
                throw new Exception ( "message" );
            }

            return _Send ( bitBuffer );
        }

        /// <summary>
        ///     Returns a connection state string.
        /// </summary>
        /// <returns>Connection state string.</returns>
        public override string ToString (){
            if (Socket == null){
                return "No Socket!";
            }
            else{
                return Socket.RemoteEndPoint != null ? Socket.RemoteEndPoint.ToString () : "Not Connected!";
            }
        }

        #endregion

        /// <summary>
        ///     Gets underlying socket.
        /// </summary>
        public Socket Socket { get; private set; }
    }
}