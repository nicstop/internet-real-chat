﻿using System.Collections.Generic;
using System.Linq;

namespace NetworkChat.Net {
    public sealed class ConnectionDataEventArgs : ConnectionEventArgs {
        public ConnectionDataEventArgs ( IConnection connection, IEnumerable<byte> data )
            : base ( connection ){
            Data = data ?? new byte[0];
        }

        public IEnumerable<byte> Data { get; private set; }

        public override string ToString (){
            return Connection.RemoteEndPoint != null
                       ? string.Format ( "{0}: {1} bytes", Connection.RemoteEndPoint, Data.Count () )
                       : string.Format ( "Not Connected: {0} bytes", Data.Count () );
        }
    }
}