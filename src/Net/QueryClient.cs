﻿using System.Net;
using System.Net.Sockets;
using System.Text;
using NetworkChat.Net.Message;

namespace NetworkChat.Net {
    public class QueryClient {
        private static readonly byte[] endSymbol = Encoding.UTF8.GetBytes ( "<EOF>" );
        public bool IsConnected = false;
        public Socket Handler { get; private set; }

        public ClientMessage SendQuery ( BitBuffer bitBuffer ){
            try{
                Handler = new Socket ( AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp );
                Handler.SendTimeout = 2000;
                Handler.ReceiveTimeout = 2000;

                Handler.Connect(new IPEndPoint(IPAddress.Parse("86.106.252.188"), 3434));

                IsConnected = true;

                var connection = new Connection ( Handler );
                connection._Send ( bitBuffer );

                byte[] buffer = connection.ReceiveBigData ( endSymbol );

                return new ClientMessage ().Parse ( buffer, 0, buffer.Length - endSymbol.Length );
            }
            catch (SocketException){
                Handler.Close ();
                IsConnected = false;
            }

            return null;
        }

        public void Close ( bool reuseSocket ){
            Handler.Disconnect ( reuseSocket );
        }
    }
}