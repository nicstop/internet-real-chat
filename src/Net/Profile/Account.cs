﻿namespace NetworkChat.Net.Profile {
    internal static class Account {
        internal static string Name { get; set; }

        internal static string Password { get; set; }

        internal static string AccessLevel { get; set; }
    }
}