﻿using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using NetworkChat.Net.Message;

namespace NetworkChat.Net {
    public interface IConnection {
        // Read bytes from the Sokcet into the buffer in a non-blocking call.
        // This allows us to read no more than the specified count number of bytes.

        // Expose the RecvBuffer.
        byte[] RecvBuffer { get; }

        /// <summary>
        ///     Returns true if there exists an active connection.
        /// </summary>
        bool IsConnected { get; }

        /// <summary>
        ///     Returns remote endpoint.
        /// </summary>
        IPEndPoint RemoteEndPoint { get; }

        /// <summary>
        ///     Returns local endpoint.
        /// </summary>
        IPEndPoint LocalEndPoint { get; }

        /// <summary>
        ///     Gets underlying socket.
        /// </summary>
        Socket Socket { get; }

        int Receive ( int start, int count );

        /// <summary>
        ///     Sends byte buffer to remote endpoint.
        /// </summary>
        int _Send ( BitBuffer msg );

        /// <summary>
        ///     Sends byte buffer to remote endpoint.
        /// </summary>
        /// <param name="buffer">Byte buffer to send.</param>
        /// <param name="start">Start index to read from buffer.</param>
        /// <param name="count">Count of bytes to send.</param>
        /// <param name="flags">Sockets flags to use.</param>
        /// <returns>Returns count of sent bytes.</returns>
        int _Send ( byte[] buffer, int start, int count, SocketFlags flags );

        /// <summary>
        ///     Sends byte buffer to remote endpoint.
        /// </summary>
        /// <param name="buffer">Byte buffer to send.</param>
        /// <returns>Returns count of sent bytes.</returns>
        int Send ( byte[] buffer );

        /// <summary>
        ///     Sends byte buffer to remote endpoint.
        /// </summary>
        /// <param name="buffer">Byte buffer to send.</param>
        /// <param name="flags">Sockets flags to use.</param>
        /// <returns>Returns count of sent bytes.</returns>
        int Send ( byte[] buffer, SocketFlags flags );

        /// <summary>
        ///     Sends byte buffer to remote endpoint.
        /// </summary>
        /// <param name="buffer">Byte buffer to send.</param>
        /// <param name="start">Start index to read from buffer.</param>
        /// <param name="count">Count of bytes to send.</param>
        /// <returns>Returns count of sent bytes.</returns>
        int Send ( byte[] buffer, int start, int count );

        /// <summary>
        ///     Sends byte buffer to remote endpoint.
        /// </summary>
        /// <param name="buffer">Byte buffer to send.</param>
        /// <param name="start">Start index to read from buffer.</param>
        /// <param name="count">Count of bytes to send.</param>
        /// <param name="flags">Sockets flags to use.</param>
        /// <returns>Returns count of sent bytes.</returns>
        int Send ( byte[] buffer, int start, int count, SocketFlags flags );

        /// <summary>
        ///     Sends an enumarable byte buffer to remote endpoint.
        /// </summary>
        /// <param name="data">Enumrable byte buffer to send.</param>
        /// <returns>Returns count of sent bytes.</returns>
        int Send ( IEnumerable<byte> data );

        /// <summary>
        ///     Sends an enumarable byte buffer to remote endpoint.
        /// </summary>
        /// <param name="data">Enumrable byte buffer to send.</param>
        /// <param name="flags">Sockets flags to use.</param>
        /// <returns>Returns count of sent bytes.</returns>
        int Send ( IEnumerable<byte> data, SocketFlags flags );

        /// <summary>
        ///     Kills the connection to remote endpoint.
        /// </summary>
        void Disconnect ();
    }
}