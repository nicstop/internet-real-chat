﻿using System.Linq;
using System.Text;
using NetworkChat.Common.Extensions;
using NetworkChat.Net.Profile;

namespace NetworkChat.Net.Message {
    public class BitBuffer {
        private byte[] _data;

        public BitBuffer ( byte[] data, int start, int count ){
            Data = data.Enumerate ( start, count ).ToArray ();
        }

        public BitBuffer ( byte[] data ){
            Data = data;
        }

        public BitBuffer ( Tokens token, params string[] args ){
            byte[] bytes = Encoding.UTF8.GetBytes ( ("token=" + (int) token) );

            bytes = args.Aggregate ( bytes, ( current, t ) => current.Append ( Encoding.UTF8.GetBytes ( ("&" + t) ) ) );
            Data = bytes;
        }

        public BitBuffer ( Tokens token, bool insertAccount, params string[] args ){
            byte[] bytes = Encoding.UTF8.GetBytes ( ("token=" + (int) token) );

            if (insertAccount){
                bytes =
                    bytes.Append (
                        Encoding.UTF8.GetBytes ( string.Format ( "&account_name={0}&account_password={1}", Account.Name,
                                                                 Account.Password ) ) );
            }

            bytes = args.Aggregate ( bytes, ( current, t ) => current.Append ( Encoding.UTF8.GetBytes ( ("&" + t) ) ) );
            Data = bytes;
        }

        public byte[] Data{
            get{
                byte[] endofdata = Encoding.UTF8.GetBytes ( "<EOF>" );
                return !_data.Contains ( endofdata ) ? _data.Append ( endofdata ) : _data;
            }
            set { _data = value; }
        }

        public override string ToString (){
            return Encoding.UTF8.GetString ( Data );
        }
    }
}