﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetworkChat.Net.Message {
    public class ClientMessage {
        private Dictionary<string, string> _dic = new Dictionary<string, string> ();

        public string this [ string key ]{
            get { return !_dic.ContainsKey ( key ) ? null : _dic[key]; }
            set{
                if (!_dic.ContainsKey ( key )){
                    _dic.Add ( key, value );
                }
            }
        }

        public ClientMessage Parse ( BitBuffer bitBuffer ){
            Parse ( bitBuffer.Data );
            return this;
        }

        public ClientMessage Parse ( byte[] source ){
            Parse ( source, 0, source.Length );
            return this;
        }

        public ClientMessage Parse ( byte[] source, int offest, int count ){
            Parse ( Encoding.UTF8.GetString ( source, 0, count ) );
            return this;
        }

        public ClientMessage Parse ( string source ){
            _dic = source.Split ( new[]{'&'}, StringSplitOptions.RemoveEmptyEntries )
                         .Select ( l => l.Split ( new[]{'='}, StringSplitOptions.RemoveEmptyEntries ) )
                         .ToDictionary ( s => s[0].Trim (), s => s.Count () == 1 ? "" : s[1].Trim () );

            return this;
        }

        public override string ToString (){
            string tempValue = "";

            _dic.ToList ().ForEach ( item => tempValue += "&" + (item.Key + item.Value) );

            return tempValue;
        }
    }
}