﻿using System;
using System.Linq;
using NetworkChat.Net.Message;

namespace NetworkChat.Net {
    public enum Tokens {
        SendRoomClients = 1,
        CloseMe = 2,
        ClientList = 3,
        NoticeConnection = 4,
        BanAccount = 5,
        WhoIam = 6,
        Login = 7,
        Register = 8,
        RoomList = 9,
        JoinRoom = 10,
        RemoveClient = 11,
        AddClient = 12,
        Banned = 13,
        Unban = 14,
        Kick = 15,
        Kicked = 16,
        AccountOwnerEntered = 17,
        CreateRoom = 18,
        OpenRoom = 19
    }

    public static class Token {
        public static Tokens Parse ( BitBuffer bitBuffer ){
            string tok = new ClientMessage ().Parse ( bitBuffer )["token"];

            return
                (Tokens)
                Enum.Parse ( typeof (Tokens),
                             Enum.GetName ( typeof (Tokens), Convert.ToInt32 ( tok == "" ? "0" : tok ) ) );
        }

        public static Tokens Parse ( string source ){
            if (!source.All ( Char.IsNumber )){
                throw new ArgumentException ( "source" );
            }

            return (Tokens) Enum.Parse ( typeof (Tokens), Enum.GetName ( typeof (Tokens), Convert.ToInt32 ( source ) ) );
        }
    }
}