﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Forms;

using NetworkChat.Net;
using NetworkChat.Net.Message;

using MessageBox = System.Windows.Forms.MessageBox;
using System.Windows.Input;

namespace NetworkChat {

    /// <summary>
    ///     Interaction logic for banWindow.xaml
    /// </summary>
    public partial class BanWindow {

        private readonly RoomIrc _roomIrc;

        public BanWindow(){
            InitializeComponent();
        }

        public BanWindow( RoomIrc roomIrc, string account ){
            _roomIrc = roomIrc;

            InitializeComponent();
        }

        private void btnBan_Click( object sender, RoutedEventArgs e ){
            if (tbxAccount.Text.Length < 3){
                MessageBox.Show( Properties.Resources.Too_short_account_name_,
                        Properties.Resources.Internet_Real_Chat, MessageBoxButtons.OK, MessageBoxIcon.Error );
                return;
            }

            if (!tbxPeriod.Text.All( Char.IsNumber )){
                MessageBox.Show( Properties.Resources.Time_value_must_be_number,
                        Properties.Resources.Internet_Real_Chat, MessageBoxButtons.OK, MessageBoxIcon.Error );
                return;
            }

            if (cbbTime.SelectedIndex == -1){
                MessageBox.Show( Properties.Resources.Select_banning_time,
                        Properties.Resources.Internet_Real_Chat, MessageBoxButtons.OK, MessageBoxIcon.Error );
                return;
            }

            _roomIrc.Send( new BitBuffer( Tokens.BanAccount, "data=" + tbxAccount.Text, "time=" + tbxPeriod.Text,
                    "typeofTime=" + cbbTime.SelectedIndex ) );

            ClearFields();
        }

        private void ClearFields(){
            tbxAccount.Clear();
            tbxPeriod.Clear();
            cbbTime.SelectedIndex = -1;
        }

        private void tbxAccount_GotFocus( object sender, RoutedEventArgs e ){
            if (tbxAccount.Text != (string) tbxAccount.Tag){
                return;
            }

            tbxAccount.Clear();
            tbxAccount.Foreground = System.Windows.Media.Brushes.Black;
        }

        private void tbxAccount_LostFocus( object sender, RoutedEventArgs e ){
            if (tbxAccount.Text.Length != 0){
                return;
            }

            tbxAccount.Text = (string) tbxAccount.Tag;
            tbxAccount.Foreground = System.Windows.Media.Brushes.LightGray;

        }

        private void tbxPeriod_GotFocus( object sender, RoutedEventArgs e ){
            if (tbxPeriod.Text != (string) tbxPeriod.Tag){
                return;
            }

            tbxPeriod.Clear();
            tbxPeriod.Foreground = System.Windows.Media.Brushes.Black;
        }

        private void tbxPeriod_LostFocus( object sender, RoutedEventArgs e ){
            if (tbxPeriod.Text.Length != 0){
                return;
            }

            tbxPeriod.Text = (string) tbxPeriod.Tag;
            tbxPeriod.Foreground = System.Windows.Media.Brushes.LightGray;
        }

        private void tbxPeriod_PreviewKeyDown( object sender, System.Windows.Input.KeyEventArgs e ){
            if (!char.IsNumber( e.Key.ToString(), e.Key.ToString().Length - 1 ) && e.Key != Key.Back){
                e.Handled = true;
            }
        }

    }

}