﻿#pragma checksum "..\..\UnbanWindow.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "EF115AD8F306409E1A5F934B254CEE73"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.1
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using UIElements;


namespace NetworkChat {
    
    
    /// <summary>
    /// UnbanWindow
    /// </summary>
    public partial class UnbanWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 6 "..\..\UnbanWindow.xaml"
        internal UIElements.TaskButton tButton;
        
        #line default
        #line hidden
        
        
        #line 7 "..\..\UnbanWindow.xaml"
        internal System.Windows.Controls.TextBox TbxAccount;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/NetworkChat;component/unbanwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\UnbanWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.tButton = ((UIElements.TaskButton)(target));
            
            #line 6 "..\..\UnbanWindow.xaml"
            this.tButton.Click += new System.Windows.RoutedEventHandler(this.TButton_OnClick);
            
            #line default
            #line hidden
            return;
            case 2:
            this.TbxAccount = ((System.Windows.Controls.TextBox)(target));
            
            #line 7 "..\..\UnbanWindow.xaml"
            this.TbxAccount.GotFocus += new System.Windows.RoutedEventHandler(this.TbxAccount_GotFocus);
            
            #line default
            #line hidden
            
            #line 7 "..\..\UnbanWindow.xaml"
            this.TbxAccount.LostFocus += new System.Windows.RoutedEventHandler(this.TbxAccount_LostFocus);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

