﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NetworkChat.Common.Extensions {
    public static class ArrayExtensions {
        public static IEnumerable<T> EnumerateFrom<T> ( this T[] array, int start ){
            if (array == null){
                throw new ArgumentNullException ( "array" );
            }

            return Enumerate ( array, start, array.Length );
        }

        public static IEnumerable<T> Enumerate<T> ( this T[] array, int start, int count ){
            if (array == null){
                throw new ArgumentNullException ( "array" );
            }

            for (int i = 0; i < count; i++){
                yield return array[start + i];
            }
        }

        public static IEnumerable<byte> Append ( this IEnumerable<byte> a, byte[] b ){
            byte[] c = a.ToArray ();

            var result = new byte[c.Length + b.Length];

            c.CopyTo ( result, 0 );
            b.CopyTo ( result, c.Length );

            return result;
        }

        public static byte[] Append ( this byte[] a, byte[] b ){
            var result = new byte[a.Length + b.Length];

            a.CopyTo ( result, 0 );
            b.CopyTo ( result, a.Length );

            return result;
        }

        public static bool CompareTo ( this byte[] byteArray, byte[] second ){
            if (byteArray.Length != second.Length){
                return false;
            }

            return !byteArray.Where ( ( t, i ) => second[i] != t ).Any ();
        }

        public static byte[] ToByteArray ( this string str ){
            str = str.Replace ( " ", String.Empty );

            var res = new byte[str.Length/2];
            for (int i = 0; i < res.Length; ++i){
                string temp = String.Concat ( str[i*2], str[i*2 + 1] );
                res[i] = Convert.ToByte ( temp, 16 );
            }
            return res;
        }

        public static int SearchBytePattern ( byte[] bytes, byte[] pattern, int start ){
            int matches = 0;
            for (int i = start; i < bytes.Length; i++){
                if (pattern[0] == bytes[i] && bytes.Length - i >= pattern.Length){
                    bool ismatch = true;
                    for (int j = 1; j < pattern.Length && ismatch; j++){
                        if (bytes[i + j] != pattern[j]){
                            ismatch = false;
                        }
                    }
                    if (ismatch){
                        matches++;
                        i += pattern.Length - 1;
                    }
                }
            }

            return matches;
        }

        public static bool Contains ( this byte[] array, byte[] pattern, int start ){
            // int matches = 0;

            for (int i = start; i < array.Length; i++){
                if (pattern[0] == array[i] && array.Length - i >= pattern.Length){
                    bool ismatch = true;
                    for (int j = 1; j < pattern.Length && ismatch; j++){
                        if (array[i + j] != pattern[j]){
                            ismatch = false;
                        }
                    }
                    if (ismatch){
                        // matrches++;
                        return true;
                    }
                }
            }

            return false;
        }

        public static bool Contains ( this byte[] array, byte[] pattern ){
            // int matches = 0;

            for (int i = 0; i < array.Length; i++){
                if (pattern[0] == array[i] && array.Length - i >= pattern.Length){
                    bool ismatch = true;
                    for (int j = 1; j < pattern.Length && ismatch; j++){
                        if (array[i + j] != pattern[j]){
                            ismatch = false;
                        }
                    }
                    if (ismatch){
                        // matrches++;
                        return true;
                    }
                }
            }

            return false;
        }

        public static IEnumerable<byte[]> MakeBlocks ( this byte[] Data, int blockSize ){
            int bArrayLenght = Data.Length;

            byte[] bReturn = null;

            int i = 0;

            for (; bArrayLenght > (i + 1)*blockSize; i++){
                bReturn = new byte[blockSize];
                Array.Copy ( Data, i*blockSize, bReturn, 0, blockSize );
                yield return bReturn;
            }

            int intBufforLeft = bArrayLenght - i*blockSize;

            if (intBufforLeft > 0){
                bReturn = new byte[intBufforLeft];
                Array.Copy ( Data, i*blockSize, bReturn, 0, intBufforLeft );

                yield return bReturn;
            }
        }
    }
}