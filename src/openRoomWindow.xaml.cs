﻿using System.Windows;
using System.Windows.Forms;
using NetworkChat.Net;
using NetworkChat.Net.Message;
using MessageBox = System.Windows.Forms.MessageBox;

namespace NetworkChat {
    /// <summary>
    ///     Interaction logic for openRoomWindow.xaml
    /// </summary>
    public partial class OpenRoomWindow : Window {
        public OpenRoomWindow (){
            InitializeComponent ();
        }

        private void btnOpen_Click ( object sender, RoutedEventArgs e ){
            if (tbxName.Text.Length < 3){
                MessageBox.Show( Properties.Resources.Too_short_name, Properties.Resources.Internet_Real_Chat, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            ProcessRequest ();
        }

        private void ProcessRequest (){
            var queryClient = new QueryClient ();
            var response =
                queryClient.SendQuery ( new BitBuffer ( Tokens.OpenRoom, true, "name=" + tbxName.Text ) );
            if (response == null){
                MessageBox.Show( Properties.Resources.Server_is_unavailable, Properties.Resources.Internet_Real_Chat, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            switch (response["open_status"]){
                case "open":
                    MessageBox.Show( Properties.Resources.Room_was_opened__enjoy, Properties.Resources.Internet_Real_Chat, MessageBoxButtons.OK,
                                      MessageBoxIcon.Information );
                    break;
                case "not_found":
                    MessageBox.Show( Properties.Resources.Room_was_not_found_, Properties.Resources.Internet_Real_Chat, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case "already_opened":
                    MessageBox.Show( Properties.Resources.Room_is_already_opened, Properties.Resources.Internet_Real_Chat, MessageBoxButtons.OK,
                                      MessageBoxIcon.Error );
                    break;
            }
        }
    }
}