﻿using System.Windows;
using System.Windows.Forms;
using NetworkChat.Net;
using NetworkChat.Net.Message;
using MessageBox = System.Windows.Forms.MessageBox;

namespace NetworkChat {
    /// <summary>
    ///     Interaction logic for registrationWindow.xaml
    /// </summary>
    public partial class RegistrationWindow : Window {
        public RegistrationWindow (){
            InitializeComponent ();
        }

        private void btnRegister_Click ( object sender, RoutedEventArgs e ){
            if (tbAccountname.Text.Length < 4){
                MessageBox.Show( Properties.Resources.Account_name_is_too_short_, Properties.Resources.Internet_Real_Chat, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (tbAccountpassword.Password.Length < 4){
                MessageBox.Show( Properties.Resources.Password_is_too_short, Properties.Resources.Internet_Real_Chat, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (tbAccountpassword.Password != tbAccountrepeatedpassword.Password){
                MessageBox.Show( Properties.Resources.Passwords_don_t_match, Properties.Resources.Internet_Real_Chat, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var queryClient = new QueryClient ();

            ClientMessage clientMessage =
                queryClient.SendQuery ( new BitBuffer ( Tokens.Register, "account_name=" + tbAccountname.Text,
                                                        "&account_password=" + tbAccountpassword.Password ) );

            Tokens token = Token.Parse ( clientMessage["token"] == "" ? "0" : clientMessage["token"] );

            if (token != Tokens.Register){
                return;
            }

            if (clientMessage["registered"] == "0"){
                MessageBox.Show ( Properties.Resources.Account_was_registered,
                                  Properties.Resources.Internet_Real_Chat, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close ();
            }
            else{
                MessageBox.Show ( Properties.Resources.Account_name_is_occupated,
                                  Properties.Resources.Internet_Real_Chat, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            if (clientMessage["close"] == "true"){
                queryClient.Close ( false );
            }
        }
    }
}