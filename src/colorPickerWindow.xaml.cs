﻿using System.Windows;
using NetworkChat.Properties;

using MessageBox = System.Windows.Forms.MessageBox;

namespace NetworkChat {

    /// <summary>
    ///     Interaction logic for colorPickerWindow.xaml
    /// </summary>
    public partial class ColorPickerWindow {

        public ColorPickerWindow(){
            InitializeComponent();
        }

        private void taskButton1_Click( object sender, RoutedEventArgs e ){
            Settings.Default.MessageColor = ColorPicker1.SelectedColorText;
            Settings.Default.Save();
            Close();
        }

        private void RobMessage_Click( object sender, RoutedEventArgs e ){
        }

    }

}