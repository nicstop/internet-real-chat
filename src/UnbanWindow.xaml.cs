﻿using System.Windows;
using System.Windows.Media;
using NetworkChat.Net;
using NetworkChat.Net.Message;

namespace NetworkChat {

    /// <summary>
    /// Interaction logic for UnbanWindow.xaml
    /// </summary>
    public partial class UnbanWindow {

        private readonly RoomIrc _roomIrc;

        public UnbanWindow(RoomIrc roomIrc){
            InitializeComponent();
            _roomIrc = roomIrc;
        }

        private void TbxAccount_GotFocus( object sender, RoutedEventArgs e ){
            if (TbxAccount.Text != (string) TbxAccount.Tag){
                return;
            }
            TbxAccount.Clear();
            TbxAccount.Foreground = Brushes.Black;
        }

        private void TbxAccount_LostFocus( object sender, RoutedEventArgs e ){
            if (TbxAccount.Text.Length != 0){
                return;
            }

            TbxAccount.Text = (string) TbxAccount.Tag;
            TbxAccount.Foreground = Brushes.LightGray;
        }

        private void TButton_OnClick( object sender, RoutedEventArgs e ){
            _roomIrc.Send(new BitBuffer(Tokens.Unban, "data=" + TbxAccount.Text));
            TbxAccount.Clear();
        }
    }

}
