﻿using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using NetworkChat.Net;
using NetworkChat.Net.Message;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using MessageBox = System.Windows.Forms.MessageBox;

namespace NetworkChat {
    /// <summary>
    ///     Interaction logic for CreateRoomWindow.xaml
    /// </summary>
    public partial class CreateRoomWindow : Window {
        public CreateRoomWindow (){
            InitializeComponent ();
        }

        private void btnSend_Click ( object sender, RoutedEventArgs e ){
            if (tbxName.Text.Length < 4){
                MessageBox.Show(Properties.Resources.Name_is_too_short, Properties.Resources.Internet_Real_Chat, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (tbxCapacity.Text.Length == 0){
                MessageBox.Show(Properties.Resources.Capacity_value_error, Properties.Resources.Internet_Real_Chat, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (int.Parse ( tbxCapacity.Text ) > 255){
                MessageBox.Show(Properties.Resources.Capacity_value_must_be___256, Properties.Resources.Internet_Real_Chat, MessageBoxButtons.OK,
                                  MessageBoxIcon.Error );
                return;
            }

            var queryClient = new QueryClient ();
            ClientMessage response =
                queryClient.SendQuery ( new BitBuffer ( Tokens.CreateRoom, true, "name=" + tbxName.Text,
                                                        "password=" + pbxPassword.Password,
                                                        "capacity=" + tbxCapacity.Text ) );

            if (response == null){
                MessageBox.Show(Properties.Resources.Server_is_unavalible__try_later, Properties.Resources.Internet_Real_Chat, MessageBoxButtons.OK,
                                  MessageBoxIcon.Error );
                return;
            }

            switch (response["create_status"]){
                case "invalid_access_level":
                    MessageBox.Show(Properties.Resources.You_have_no_access_to_this_command, Properties.Resources.Internet_Real_Chat, MessageBoxButtons.OK,
                                      MessageBoxIcon.Error );
                    break;
                case "invalid_name":
                    MessageBox.Show( Properties.Resources.Invalid_room_name, Properties.Resources.Internet_Real_Chat, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case "name_is_occupated":
                    MessageBox.Show( Properties.Resources.This_name_is_occupated, Properties.Resources.Internet_Real_Chat, MessageBoxButtons.OK,
                                      MessageBoxIcon.Error );
                    break;
                case "too_long_name":
                    MessageBox.Show( Properties.Resources.Room_name_is_too_long, Properties.Resources.Internet_Real_Chat, MessageBoxButtons.OK,
                                      MessageBoxIcon.Error );
                    break;
                case "password_is_too_long":
                    MessageBox.Show( Properties.Resources.Password_is_too_long, Properties.Resources.Internet_Real_Chat, MessageBoxButtons.OK,
                                      MessageBoxIcon.Error );
                    break;
                case "cpacity_value_error":
                    MessageBox.Show( Properties.Resources.Capacity_value_error, Properties.Resources.Internet_Real_Chat, MessageBoxButtons.OK,
                                      MessageBoxIcon.Error );
                    break;
                case "capacity_value_must_be_positive":
                    MessageBox.Show( Properties.Resources.Capacity_value_must_be_positive, Properties.Resources.Internet_Real_Chat, MessageBoxButtons.OK,
                                      MessageBoxIcon.Error );
                    break;
                case "room_was_created":
                    MessageBox.Show( Properties.Resources.Room_was_created__enjoy_, Properties.Resources.Internet_Real_Chat, MessageBoxButtons.OK,
                                      MessageBoxIcon.Information );
                    break;
            }
        }

        private void tbxCapacity_PreviewKeyDown ( object sender, KeyEventArgs e ){
            if (!char.IsNumber ( e.Key.ToString (), e.Key.ToString ().Length - 1 ) && e.Key != Key.Back){
                e.Handled = true;
            }
        }
    }
}