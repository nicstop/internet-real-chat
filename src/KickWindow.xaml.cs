﻿using System.Windows;
using NetworkChat.Net;
using NetworkChat.Net.Message;

namespace NetworkChat {

    /// <summary>
    /// Interaction logic for KickWindow.xaml
    /// </summary>
    public partial class KickWindow {

        private readonly RoomIrc _roomIrc;

        public KickWindow(){
            
        }

        public KickWindow( RoomIrc roomIrc ){
            InitializeComponent();
            _roomIrc = roomIrc;
        }

        private void tbxAccount_GotFocus( object sender, RoutedEventArgs e ){

            if (tbxAccount.Text != (string) tbxAccount.Tag){
                return;
            }
            tbxAccount.Clear();
            tbxAccount.Foreground = System.Windows.Media.Brushes.Black;
        }

        private void tbxAccount_LostFocus( object sender, RoutedEventArgs e ){
            if (tbxAccount.Text.Length != 0){
                return;
            }

            tbxAccount.Text = (string) tbxAccount.Tag;
            tbxAccount.Foreground = System.Windows.Media.Brushes.LightGray;
        }

        private void TbKick_Click( object sender, RoutedEventArgs e ){
            _roomIrc.Send( new BitBuffer( Tokens.Kick, "data=" + tbxAccount.Text ) );
            tbxAccount.Clear();
        }

    }

}
