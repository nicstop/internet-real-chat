﻿using System;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using NetworkChat.Net;
using NetworkChat.Net.Message;
using NetworkChat.Net.Profile;
using MessageBox = System.Windows.Forms.MessageBox;

namespace NetworkChat {
    /// <summary>
    ///     Interaction logic for RoomWindow.xaml
    /// </summary>
    public partial class RoomWindow {
        public RoomWindow (){
            InitializeComponent ();
        }

        private QueryClient QueryClient { get; set; }

        private void roomLtw_Loaded ( object sender, RoutedEventArgs e ){
            QueryClient = new QueryClient ();

            if (Account.AccessLevel == "1"){
                Menu.Visibility = Visibility.Visible;
            }

            RefreshList ();
        }

        private void Window_Unloaded ( object sender, RoutedEventArgs e ){
            Owner.Close ();
        }

        public void RefreshList (){
            roomLtw.Items.Clear ();

            var queryClient = new QueryClient ();
            var clientMessage = queryClient.SendQuery ( new BitBuffer ( Tokens.RoomList, true ) );

            if (clientMessage == null){
                MessageBox.Show( Properties.Resources.Server_is_unavalible__try_later, Properties.Resources.Internet_Real_Chat, MessageBoxButtons.OK,
                                  MessageBoxIcon.Error );
                return;
            }

            if (string.IsNullOrEmpty ( clientMessage["rooms_total"] )){
                return;
            }

            var totalRooms = int.Parse ( clientMessage["rooms_total"] );
            for (int i = 0; i < totalRooms; ++i){
                try{
                    var roomInfo = clientMessage["rm" + i].Split ( new[]{","},
                                                                        StringSplitOptions.RemoveEmptyEntries );
                    roomLtw.Items.Add ( new{Room = roomInfo[0], InRoom = roomInfo[1], Password = roomInfo[2]} );
                }
                catch (NullReferenceException){
                    break;
                }
            }
        }

        private void btnRefresh_Click ( object sender, RoutedEventArgs e ){
            RefreshList ();
        }

        private void roomLtw_MouseDoubleClick ( object sender, MouseButtonEventArgs e ){
            if (roomLtw.SelectedItem == null){
                return;
            }

            string roomData = roomLtw.SelectedItems[0].ToString ();

            roomData =
                roomData.Substring ( roomData.IndexOf ( '{' ) + 1, roomData.IndexOf ( ',' ) - 1 )
                        .Split ( new[]{'='}, StringSplitOptions.RemoveEmptyEntries )[1].Trim ();

            var clientMessage =
                QueryClient.SendQuery ( new BitBuffer ( Tokens.JoinRoom, true, "room_name=" + roomData, "room_password=" ) );
            ProcessJoin ( clientMessage, roomData );
        }

        private void ProcessJoin ( ClientMessage clientMessage, string room ){
            var token = Token.Parse ( clientMessage["token"] == "" ? "0" : clientMessage["token"] );

            if (token != Tokens.JoinRoom){
                return;
            }

            switch (clientMessage["join_status"]){
                case "room_not_found":
                    MessageBox.Show ( Properties.Resources.Room_wasn_t_found__refresh_rooms_list_,
                                      Properties.Resources.Internet_Real_Chat, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case "require_password":
                    new PasswordWindow ( QueryClient, room).ShowDialog ();
                    break;
                case "room_full":
                    MessageBox.Show ( Properties.Resources.Room_is_full_,
                                      Properties.Resources.Internet_Real_Chat, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case "banned":
                    MessageBox.Show ( Properties.Resources.You_are_banned_from_this_room__Ban_expire_in_ + clientMessage["time"],
                                      Properties.Resources.Internet_Real_Chat, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case "name_ocupated":
                    MessageBox.Show( Properties.Resources.This_is_already_taken, Properties.Resources.Internet_Real_Chat);
                    break;
                case "enter_room":
                    Hide();
                    var window = new ChatWindow( new Connection( QueryClient.Handler ),
                            room ){
                                    Owner = this
                            };
                    window.Unloaded += ( sender, e ) => Show();
                    window.ShowDialog();
                    return;
            }
        }

        private void CreateRoom_Click ( object sender, RoutedEventArgs e ){
            var window = new CreateRoomWindow {Owner = this};
            window.ShowDialog ();
        }

        private void OpenRoom_Click ( object sender, RoutedEventArgs e ){
            var window = new OpenRoomWindow {Owner = this};
            window.ShowDialog ();
        }
    }
}